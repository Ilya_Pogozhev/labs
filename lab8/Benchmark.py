import time
import lib_count
import random
import matplotlib.pyplot as plt


num_data = []
time_data = []
num_repeat = int(10e4)
all_time = 0

for i in range(1, 11):
    for n in range(3):
        coordinates = [(random.uniform(-50, 50), random.uniform(-50, 50)) for point in range(num_repeat)]
        center = (random.uniform(-50, 50), random.uniform(-50, 50))
        radius = random.uniform(-50, 50)
        start = time.time()
        count = lib_count.search(coordinates, center, radius)
        stop = time.time()
        all_time += (stop - start)
    coordinates = []
    time_data.append(all_time/3)
    num_data.append(num_repeat)
    print(all_time/3)

    file = open("program_time", 'a')
    file.write(f'{(all_time / 3):.1f}\n')
    num_repeat += int(100e3) * i

plt.plot(num_data, time_data, 'o')
plt.plot(num_data, time_data, 'b', c='red')

plt.show()

