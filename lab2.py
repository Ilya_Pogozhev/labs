# -*- coding: utf8 -*-
import math
try:
    a = float(input("Введите а "))
    x = float(input("Введите x "))
    c = int(input("Введите команду 1-g, 2-f, 3-y. "))
except ValueError:
    print("Ввод некорректен")
    exit(1)


if c == 1:
    try:
        g = (5 * (10 * a ** 2 - 11 * a * x + x ** 2)) / (24 * a ** 2 - 49 * a * x + 15 * x ** 2)
    except ZeroDivisionError:
        print("Деление на 0")
        exit(1)
    print(g)

elif c == 2:
    try:
        f = math.sinh(3 * a ** 2 + 7 * a * x + 4 * x ** 2)  # гиперболический синус
    except ValueError:
        print("Не входит в область определения")
        exit(1)
    print(f)

elif c == 3:
    try:
        y = -math.atanh(30 * a ** 2 + 37 * a * x - 4 * x ** 2)  # гиперболический арктангенс
    except ValueError:
        print("Не входит в область определения")
        exit(1)
    print(y)
else:
    print("Функция не выбрана или выбрана неправильно")
    
    




