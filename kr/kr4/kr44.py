import random
import time
from lib_for_kr4 import matrix_calculation
import matplotlib.pyplot as plt

# размер матриц и шаг
max = 100000
min = 1000
step = 10000

matrix_array = []
matrix_array_float = []
middle_time = []
middle_time2 = []

for n in range(min, max, step):
    #Генерируем  матрицы
    matrix_a = [[random.randint(1, 10) for j in range(int(n ** 0.5))] for i in range(int(n ** 0.5))]
    matrix_b = [[random.randint(1, 10) for j in range(int(n ** 0.5))] for i in range(int(n ** 0.5))]

    time_calculating = []
    # Среднее время
    for _ in range(3):
        start = time.time()
        matrix_calculation(matrix_a, matrix_b)
        stop = time.time()
        time_calculating.append(stop - start)

    middle_time.append(sum(time_calculating) / 3)
    matrix_array.append(n)

    matrix_a = [[random.uniform(1, 10) for j in range(int(n ** 0.5))] for i in range(int(n ** 0.5))]
    matrix_b = [[random.uniform(1, 10) for j in range(int(n ** 0.5))] for i in range(int(n ** 0.5))]

    time_calculating2 = []
    # Среднее время
    for _ in range(3):
        start = time.time()
        matrix_calculation(matrix_a, matrix_b)
        stop = time.time()
        time_calculating2.append(stop - start)

    middle_time2.append(sum(time_calculating2) / 3)
    matrix_array_float.append(n)

plt.plot(matrix_array, middle_time, 'o', c='red')
plt.plot(matrix_array, middle_time, 'b', c='red')

plt.plot(matrix_array_float, middle_time2, 'o', c='black')
plt.plot(matrix_array_float, middle_time2, 'b', c='black')

plt.title('График роста вектора от размерности матриц ')
plt.xlabel('Количество элементов матрицы ')
plt.ylabel('Время, с')
plt.show()
